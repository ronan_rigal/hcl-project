package com.hcl.test.services;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;
//import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hcl.test.mappers.Transaction;
import com.hcl.test.mappers.Wrapper;


@RestController
@RequestMapping("/transactions")
public class ListTransactions {	
	
	private static final Logger logger = LoggerFactory.getLogger(ListTransactions.class);
	
	//Services that transforms the data provide in the Transaction list service, in the new structure required
	@GetMapping(path = "/list", produces = "application/json")
	public ResponseEntity<String> completeList() throws JsonParseException, JsonMappingException, MalformedURLException, IOException {
		logger.info("Started completeList service execution!");
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		logger.info("Deserealizing Json");
		Wrapper wrapper = objectMapper.readValue(new  URL("https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions"), Wrapper.class);
		//Set pretty printing of json
    	objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
      	logger.info("Finish completeList service execution!");
		return ResponseEntity
	            .ok()
	            .body(objectMapper.writeValueAsString(wrapper.getTransactions()));
	}
	
	//Services that transforms the data provide in the Transaction list service, in the new structure required filtering by the Transaction Type parameter
	@GetMapping(path = "/type", produces = "application/json")
	public ResponseEntity<String> filteredList(@RequestParam String type) throws JsonParseException, JsonMappingException, MalformedURLException, IOException{
		logger.info("Started filteredList execution!");	
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		logger.info("Deserealizing Json at service filteredList");
		Wrapper wrapper = objectMapper.readValue(new  URL("https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions"), Wrapper.class);
		
		//Set pretty printing of json
    	objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	String json ="";
		try {
			logger.info("Filtering transaction list by parameter!");
			List<Transaction> transactions = wrapper.getTransactions().stream()
				.filter(t ->t.getTransactionType()!=null && t.getTransactionType().equalsIgnoreCase(type))										
				.collect(Collectors.toList());
			json = objectMapper.writeValueAsString(transactions);
		}catch(NullPointerException e) {
			logger.error("Error at filteredList: " + e.getMessage());			
		}
		logger.info("Finish filteredList service execution!");
		return ResponseEntity
	            .ok()
	            .body(json);
	}

	//Services that retrieve the total amount for transaction type resulting of filtering the provide transaction list by this parameter
	@GetMapping(path = "/amount", produces = "text/plain")//MediaType.TEXT_PLAIN)
	public ResponseEntity<String> amountForType(@RequestParam String type) throws JsonParseException, JsonMappingException, MalformedURLException, IOException{
		
		logger.info("Started amountForType execution!");	
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		logger.info("Deserealizing Json at service amountForType");
		Wrapper wrapper = objectMapper.readValue(new  URL("https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions"), Wrapper.class);
		
		//Set pretty printing of json
    	objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	double total = 0;
    			
		try {
			logger.info("Filtering transaction list by parameter at service amountForType!");
			List<Transaction> transactions = wrapper.getTransactions().stream()
				.filter(t ->t.getTransactionType()!=null && t.getTransactionType().equalsIgnoreCase(type))										
				.collect(Collectors.toList());
			logger.info("Calculating total amount for type of transaction!");
			for(Transaction tran : transactions) {
				total+=tran.getTransactionAmount();
			}
		}catch(NullPointerException e) {
			logger.error("Error at amountForType: " + e.getMessage());			
		}
		logger.info("Finish amountForType service execution!");
		return ResponseEntity
	            .ok()
	            .body(String.valueOf(total));
	}	
	
}
