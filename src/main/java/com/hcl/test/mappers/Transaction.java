package com.hcl.test.mappers;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Transaction {


    private String id;
    private String accountId;
    private String counterpartyAccount;
    private String counterpartyName;
    private String counterPartyLogoPath;
    private Double instructedAmount;
    private String instructedCurrency;
    private Double transactionAmount;
    private String transactionCurrency;
    private String transactionType;
    private String description;

    public Transaction() {

    }

    public Transaction(String id, String accId, String cpAcc, String cpName, String cpLogoPath, 
    					double instAmount, String instCurrency, double tranAmount, String tranCurrency,
    					String tranType, String desc) {
    	
    	this.id = id;
    	this.accountId = accId;
    	this.counterpartyAccount = cpAcc;
    	this.counterpartyName = cpName;
    	this.counterPartyLogoPath = cpLogoPath;
    	this.instructedAmount = instAmount;
    	this.instructedCurrency = instCurrency;
    	this.transactionAmount = tranAmount;
    	this.transactionCurrency = tranCurrency;
    	this.transactionType = tranType;
    	this.description = desc;
    }
    
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    //@JsonProperty("this_account.id")
    public String getAccountId() {
        return accountId;
    }

   // @JsonProperty("this_account.id")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    //@JsonProperty("other_account.number")
    public String getCounterpartyAccount() {
        return counterpartyAccount;
    }

    //@JsonProperty("other_account.number")
    public void setCounterpartyAccount(String counterpartyAccount) {
        this.counterpartyAccount = counterpartyAccount;
    }

    //@JsonProperty("other_account.holder.name")
    public String getCounterpartyName() {
        return counterpartyName;
    }

    //@JsonProperty("other_account.holder.name")
    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    //@JsonProperty("other_account.metadata.image_URL")
    public String getCounterPartyLogoPath() {
        return counterPartyLogoPath;
    }

    //@JsonProperty("other_account.metadata.image_URL")
    public void setCounterPartyLogoPath(String counterPartyLogoPath) {
        this.counterPartyLogoPath = counterPartyLogoPath;
    }

    //@JsonProperty("details.value.amount")
    public Double getInstructedAmount() {
        return instructedAmount;
    }

    //@JsonProperty("details.value.amount")
    public void setInstructedAmount(Double instructedAmount) {
        this.instructedAmount = instructedAmount;
    }

   // @JsonProperty("details.value.currency")
    public String getInstructedCurrency() {
        return instructedCurrency;
    }

    //@JsonProperty("details.value.currency")
    public void setInstructedCurrency(String instructedCurrency) {
        this.instructedCurrency = instructedCurrency;
    }

    //@JsonProperty("details.value.amount")
    public Double getTransactionAmount() {
        return transactionAmount;
    }

    //@JsonProperty("details.value.amount")
    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
    //@JsonProperty("details.value.currency")
    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    //@JsonProperty("details.value.currency")
    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

   // @JsonProperty("details.type")
    public String getTransactionType() {
        return transactionType;
    }

    //@JsonProperty("details.type")
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    //@JsonProperty("details.description")
    public String getDescription() {
        return description;
    }

    //@JsonProperty("details.description")
    public void setDescription(String description) {
        this.description = description;
    }
    
    @JsonProperty("this_account")
    private void unpackAccount(Map<String,Object> account) {
    	this.accountId = (String) account.get("id");
	} 

    @SuppressWarnings("unchecked")
    @JsonProperty("other_account")
    private void unpackOtherAccount(Map<String,Object> otherAcc) {
    	this.counterpartyAccount = (String) otherAcc.get("number");
    	Map<String,String> holder = (Map<String,String>) otherAcc.get("holder");
    	this.counterpartyName = holder.get("name");
        Map<String,Object> metadata = (Map<String,Object>) otherAcc.get("metadata");
        this.counterPartyLogoPath = (String)metadata.get("image_URL");
    }
    
    @SuppressWarnings("unchecked")
    @JsonProperty("details")
    private void unpackDetails(Map<String,Object> details) {
        
    	Map<String,Object> value = (Map<String,Object>) details.get("value");
    	this.instructedAmount = Double.valueOf((String)value.get("amount"));
    	this.instructedCurrency = (String) value.get("currency");
    	this.transactionAmount = this.instructedAmount;//(Double) value.get("amount");
    	this.transactionCurrency = this.instructedCurrency;//(String) value.get("currency");
    	this.transactionType = (String) details.get("type");
    	this.description = (String) details.get("description");
    }
    
    public String toString() {
	    return "[" + id +" "+ accountId +" "+ counterpartyAccount+
	               " "+ counterpartyName+" "+ counterPartyLogoPath +
	               " "+ instructedAmount +" "+ instructedCurrency +
	               " "+ transactionAmount +" "+transactionCurrency +
	               " "+transactionType+ " "+ description+"]";
	}
}
