package com.hcl.test.mappers;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wrapper {

	private List<Transaction> transactions;

	@JsonProperty("transactions")
	public List<Transaction> getTransactions() {
		return transactions;
	}

	@JsonProperty("transactions")
	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	
}
