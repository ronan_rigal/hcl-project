package com.hcl.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.security.access.annotation.Secured;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.hcl.test.services.ListTransactions;

public class ListTransactionsTest {

	String type;
	
	public ListTransactionsTest() {
		
	}
	
    @InjectMocks
    private ListTransactions listTransactions;

    private MockMvc mockMvc;
	
	@Before
	public void init() {
	    MockitoAnnotations.initMocks(this);
	    this.mockMvc = MockMvcBuilders.standaloneSetup(listTransactions).build();		
		type = "SANDBOX_TAN";
	}
	
	@Secured("authenticated")
	@Test
	public void completeListTest() throws Exception {
		//ListTransactions trans = new ListTransactions();
		//ResponseEntity<String> r = trans.completeList();
		//assertEquals(HttpStatus.OK.value(), r.getStatusCode().value());
		
		this.mockMvc.perform(get("/transactions/list")).andExpect(status().isOk());
	}
	
	@Test
	public void filteredListTest() throws Exception {
		/*ListTransactions trans = new ListTransactions();
		ResponseEntity<String> r = trans.filteredList(type);
		assertEquals(HttpStatus.OK.value(), r.getStatusCode().value());
		type = "sandbox-payment";
		r = trans.filteredList(type);
		assertEquals(HttpStatus.OK.value(), r.getStatusCode().value());*/
		
		this.mockMvc.perform(get("/transactions/type?type="+type)).andExpect(status().isOk());		
		
	}	
	
	@Test
	public void amountForTypeTest() throws Exception {
		/*ListTransactions trans = new ListTransactions();
		ResponseEntity<String> r = trans.filteredList(type);
		assertEquals(HttpStatus.OK.value(), r.getStatusCode().value());		
		type = "sandbox-payment";
		r = trans.filteredList(type);
		assertEquals(HttpStatus.OK.value(), r.getStatusCode().value());*/
		
		this.mockMvc.perform(get("/transactions/amount?type="+type)).andExpect(status().isOk());
	}	
	
}
