package com.hcl.test;

import static org.junit.Assert.assertEquals;

//import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
//import org.junit.jupiter.api.Test;
import org.junit.Test;

import com.hcl.test.mappers.Transaction;

public class TransactionTest {

    public static final String EXPECTED_ID = "897706c1-dcc6-4e70-9d85-8a537c7cbf3e";
    public static final String EXPECTED_ACCOUNT_ID = "savings-kids-john";
    public static final String EXPECTED_COUNTER_PARTY_ACCOUNT = "savings-kids-john";
    public static final String EXPECTED_COUNTER_PARTY_NAME = "ALIAS_49532E";
    public static final String EXPECTED_COUNTER_PARTY_LOGO_PATH = "/logos";
    public static final Double EXPECTED_INSTRUCTED_AMOUNT = -90.0;
    public static final String EXPECTED_INSTRUCTED_CURRENCY = "GBP";;
    public static final Double EXPECTED_TRANSACTION_AMOUNT = -90.0;
    public static final String EXPECTED_TRANSACTION_CURRENCY = "GBP";
    public static final String EXPECTED_TRANSACTION_TYPE = "SANDBOX_TAN";
    public static final String EXPECTED_DESCRIPTION = "Gift";    
    
    private Transaction tran;
    
    public TransactionTest() {
    	
    }
    
    @Before
    public void init() throws Exception {
        this.tran = new Transaction("897706c1-dcc6-4e70-9d85-8a537c7cbf3e",	"savings-kids-john", "savings-kids-john",
        				"ALIAS_49532E","/logos",-90.0,"GBP", -90.0, "GBP", "SANDBOX_TAN","Gift");
    }

    @Test
    public void testTransactionDetails() throws Exception {
        assertEquals(EXPECTED_ID, this.tran.getId());
        assertEquals(EXPECTED_ACCOUNT_ID, this.tran.getAccountId());
        assertEquals(EXPECTED_COUNTER_PARTY_ACCOUNT, this.tran.getCounterpartyAccount());
        assertEquals(EXPECTED_COUNTER_PARTY_NAME, this.tran.getCounterpartyName());
        assertEquals(EXPECTED_COUNTER_PARTY_LOGO_PATH, this.tran.getCounterPartyLogoPath());
        assertEquals(EXPECTED_INSTRUCTED_AMOUNT, this.tran.getInstructedAmount());
        assertEquals(EXPECTED_INSTRUCTED_CURRENCY, this.tran.getInstructedCurrency());
        assertEquals(EXPECTED_TRANSACTION_AMOUNT, this.tran.getTransactionAmount());
        assertEquals(EXPECTED_TRANSACTION_CURRENCY, this.tran.getTransactionCurrency());
        assertEquals(EXPECTED_TRANSACTION_TYPE, this.tran.getTransactionType());
        assertEquals(EXPECTED_DESCRIPTION, this.tran.getDescription());
        
    }
}
