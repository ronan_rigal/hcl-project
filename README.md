Hcl Project

Instructions

To build a Jar file and execute the application:

	1.	Extract the file “hcltest.rar” in any directory.
	2.	Open a console (CMD in the Windows operating system) and place it inside the "hcltest" directory of the project.
	3.	From there execute the command “mvnw clean package”
	4.	Look for the Jar file “hcltest-0.0.1-SNAPSHOT.jar”  in the directory “hcltest/target” and execute it.

To run the application from the console:

	5.	Skip the steps 3 and 4. From this directory execute the command “mvn spring-boot:run”

After any of the two methods detailed before, You can test the application following these steps:

	1.	Open in your browser the URL: https://localhost:8443/login
	2.	You have to login for get access to any of the endpoints of the services.
		2.1	user: admin
		2.2	password: admin
	3.	After that You can execute the following URLs of the services in your browser:
		3.1	Transaction List https://localhost:8443/transactions/list
		3.2	Transaction filter based on transaction type(Query-Parameter) https://localhost:8443/transactions/type?type=  (after the = You should enter your param value)
		3.3	Total amount for transaction type(Query-Parameter)  https://localhost:8443/transactions/amount?type=  (after the = You should enter your param value)
